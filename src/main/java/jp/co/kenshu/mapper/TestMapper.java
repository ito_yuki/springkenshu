package jp.co.kenshu.mapper;

import java.util.List;

import jp.co.kenshu.dto.test.TestDto;
import jp.co.kenshu.entity.Test;

public interface TestMapper {
	Test getTest(Integer id);
	Test getTestByDto(TestDto dto);
	Integer insertTest(String name);
	Integer deleteTest(Integer id);
	Integer updateTest(TestDto dto);
	//トランザクションテスト用の失敗メソッド
	Integer insertFailTest(Object object);
	List<Test> getTestAll();
}
