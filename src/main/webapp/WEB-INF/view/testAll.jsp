<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
    <head>
        <meta charset="utf-8">
        <title>武田鉄矢</title>
    </head>
    <body>
           <a href="insert/input/">鉄矢を追加</a> <a href="delete/input/">鉄矢を削除</a>
           <h1>${message}</h1>
           <c:forEach items="${tests}" var="test">
           		<p><c:out value="${test.id}"></c:out>:<c:out value="${test.name}"></c:out> <a href="${pageContext.request.contextPath}/test/update/input/${test.id}/">更新</a></p>
           		<c:choose>
           			<c:when test="${test.name == 'ホリ'}">
           				<img src="http://s3if1.storage.gree.jp/album/28/98/20102898/2f344c0d_240.jpg">
           			</c:when>
           			<c:otherwise>
           				<c:if test="${test.id % 3 != 0}"><img src="https://img.asagei.com/wp-content/uploads/2016/02/20160224takeda-250x250.jpg"></c:if>
           				<c:if test="${test.id % 3 == 0}"><img src="https://rr.img.naver.jp/mig?src=http%3A%2F%2Fcdn-ak.f.st-hatena.com%2Fimages%2Ffotolife%2Fo%2Fover300TB%2F20120514%2F20120514202000.jpg&twidth=1000&theight=0&qlt=80&res_format=jpg&op=r"></c:if>
           			</c:otherwise>
           		</c:choose>
           </c:forEach>
    </body>
</html>