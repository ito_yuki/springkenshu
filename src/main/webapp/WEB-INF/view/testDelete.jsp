<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
<meta charset="UTF-8">
<title>鉄矢を削除</title>
</head>
<body>
	<h1>${message}</h1>
	<form:form modelAttribute="testForm">
		ID:<form:input path="id"/>
		<input type="submit">
	</form:form>
	<a href="javascript:history.back()">削除しない</a>
</body>
</html>